package com.moueslati.generic

import com.moueslati.model.mower.{Direction, Position}

trait MowerBehaviour[Mv] {

  var position:Position
  var direction:Direction

  def changeDirection(movement:Mv) :Unit
  def move :Unit
  def nextPosition :Position
}
