package com.moueslati.generic

import com.moueslati.model.mower.Position

trait FieldBehaviour[Mw,Plc,Mv]{

  var mowers: Seq[Mw]
  var mowerToMove: Option[Mw]

  def isPlaceableIn(position:Position): Boolean
  def placeMower(placement:Plc) :Boolean
  def moveMower(movementSeq :Seq[Mv])
  def reset
}
