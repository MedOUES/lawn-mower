package com.moueslati.model.field

import com.moueslati.generic.FieldBehaviour
import com.moueslati.model.mower.{Forward, Left, Movement, Mower, Position, Right}

class Field(val width: Int, val length: Int) extends FieldBehaviour[Mower,Mower,Movement] {

      override var mowers: Seq[Mower] = Seq.empty
      override var mowerToMove: Option[Mower] = None

      override def isPlaceableIn(position:Position): Boolean = if (position.x>width || position.x<0 || position.y>length || position.y<0)
                                                                  false
                                                               else
                                                                  !mowers.exists( _.position == position)


      override def placeMower(mower: Mower): Boolean = if (isPlaceableIn(mower.position)) {
                                                        mowers = mowers :+ mower
                                                        true
                                                      }
                                                      else
                                                        false


      override def moveMower(movementSeq: Seq[Movement]): Unit = {  mowerToMove = Option(mowers(mowers.indexOf(mowerToMove.orNull) + 1))
                                                                    if (mowerToMove.isDefined)
                                                                      movementSeq.foreach(mv => moveMowerByStep(mowerToMove.get, mv))
      }


      override def reset :Unit = {  mowers = Seq.empty
                                    mowerToMove = None
      }


      def moveMowerByStep(mower: Mower, movement: Movement): Unit = movement match {
                                                                                  case rOrL @ (Right|Left) => mower.changeDirection(rOrL)
                                                                                  case Forward if isPlaceableIn(mower.nextPosition) => mower.move
                                                                                  case _ =>
      }
}


object Field {

      def apply(width: Int, length: Int) = new Field(width, length)
}
