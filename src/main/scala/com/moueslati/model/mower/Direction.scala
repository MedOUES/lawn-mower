package com.moueslati.model.mower

sealed trait Direction { self =>


  def next :Direction = self match {

                            case North => East
                            case East => South
                            case South => West
                            case West => North
                          }

  def previous :Direction = self match {

                                        case North => West
                                        case East => North
                                        case South => East
                                        case West => South
                                      }

}

case object North extends Direction
case object West extends Direction
case object South extends Direction
case object East extends Direction