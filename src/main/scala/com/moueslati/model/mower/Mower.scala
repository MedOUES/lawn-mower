package com.moueslati.model.mower

import com.moueslati.generic.MowerBehaviour

class Mower(position0:Position,direction0:Direction) extends MowerBehaviour[Movement] {

    override var position:Position = position0
    override var direction:Direction = direction0

    override def changeDirection(movement:Movement) :Unit = movement match {

                                                        case Right => direction = direction.next
                                                        case Left => direction = direction.previous
                                                        case _ =>
                                                      }

    override def move = absNextPos( pos => position = pos)
    override def nextPosition = absNextPos(identity)

    def absNextPos[T](exec: Position => T): T = {

        val pos = direction match {

          case North => Position(position.x,position.y+1)
          case East =>  Position(position.x+1,position.y)
          case South => Position(position.x,position.y-1)
          case West =>  Position(position.x-1,position.y)
        }
        exec(pos)
    }

}


object Mower {

    def apply(position0: Position, direction0: Direction): Mower = new Mower(position0, direction0)
}
