package com.moueslati.model.mower

sealed trait Movement

case object Right extends Movement
case object Left extends Movement
case object Forward extends Movement
