package com.moueslati.test.model

import com.moueslati.model.field.Field
import com.moueslati.model.mower._
import org.scalatest.{FlatSpec, Matchers}

class FieldTest extends FlatSpec with Matchers {

  var field :Field = _

  "Instantiating a field" should "produce an object with correct width/length" in {

    field = Field(7, 10)
    field.width shouldBe 7
    field.length shouldBe 10
  }
  it should "have no mowers" in {

    field.mowers shouldBe empty
  }
  it should "have no mower to move" in {

    field.mowerToMove shouldBe None
  }

  val mowerA = Mower(Position(0,0),North)
  val mowerAPos0:Position = mowerA.position.copy()
  val mowerADir0:Direction = mowerA.direction

  val mowerB = Mower(Position(6,4),East)
  val mowerBPos0:Position = mowerB.position.copy()
  val mowerBDir0:Direction = mowerB.direction


  "Adding new mower(s)" should "increase field # of mowers " in {

    field.placeMower(mowerA)
    field.placeMower(mowerB)
    field.mowers should have size 2
  }
  it should "ignore non placeable mowers" in {

    val mowerC = Mower(Position(9,4),East)
    field.placeMower(mowerC)
    field.mowers should have size 2
  }


  "Moving a mower" should "concern the first one added on the sequence" in {

    field.moveMower(Seq(Forward,Forward,Right,Forward))
    field.mowerToMove.get should be theSameInstanceAs mowerA
  }

  it should "place on the correct position and keep the right direction" in {

    mowerA.position shouldBe Position(mowerAPos0.x+1,mowerAPos0.y+2)
    mowerA.direction shouldBe mowerADir0.next
  }


  "Moving a second mower" should "point to the next one to move" in {

    field.moveMower(Seq(Forward,Forward,Right,Forward))
    field.mowerToMove.get should be  theSameInstanceAs mowerB
  }

  it should "ignore the movement that places outside of the field" in {

    mowerB.position shouldBe Position(mowerBPos0.x+1,mowerBPos0.y-1)
    mowerB.direction shouldBe mowerBDir0.next
  }


  "Reseting the field" should "empty the mowers sequence" in {

    field.reset
    field.mowers shouldBe empty
    field.mowerToMove shouldBe None
  }


}
