name := "lawn-mower"
version := "1.0-SNAPSHOT"
organization := "com.moueslati"
scalaVersion := "2.12.6"


val scalaLoggingV = "3.9.0"
val logbackV = "1.2.3"
val scalacticV  = "3.0.5"
val scalatestV  = "3.0.5"


libraryDependencies ++= Seq (
                            "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingV,
                            "ch.qos.logback" % "logback-classic" % logbackV,
                            "org.scalactic" %% "scalactic" % scalacticV,
                             "org.scalatest" %% "scalatest" % scalatestV % "test"
                          )

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"